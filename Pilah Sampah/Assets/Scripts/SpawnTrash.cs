﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


/// <summary>
/// This class handle trash spawn
/// </summary>
public class SpawnTrash : MonoBehaviour
{
   [SerializeField]  float pauseTime = 0.8f;
   [SerializeField]  float timer;

   public GameObject[] trashPrefabs;

   private void Update()
   {
      timer += Time.deltaTime;
      if (timer > pauseTime) // delay time spawn trash 
      {
         int random = Random.Range(0, trashPrefabs.Length); // get random prefabs 
         Instantiate(trashPrefabs[random],transform.position,transform.rotation); // spawn randomed prefabs 
         timer = 0; // set timer back to default 
      }
   }
}
