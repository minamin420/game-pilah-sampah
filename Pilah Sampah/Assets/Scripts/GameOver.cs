﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// This class handle game condition when score below zero
/// </summary>
public class GameOver : MonoBehaviour
{
    [SerializeField] private float timer = 0;
    [SerializeField] private float delayTime = 3;

    // Update is called once per frame
    void Update()
    {
        ResetGameCondition();
    }

    private void ResetGameCondition()
    {
        timer += Time.deltaTime;
        if (timer > delayTime) 
        {
            Data.Score = Data.defaultscore; // set score to default 
            SceneManager.LoadScene("MainLevel"); // loadscene 
        }
    }
    
}
