
using System;
using UnityEngine;

/// <summary>
/// This Class will handle input from player to exit the game 
/// </summary>
public class Exit : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape)) // button escape 
        {
            Application.Quit(); // quit application when button escape press 
            Debug.Log("Quit");
        }
    }
}
