﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to store static variable for player score 
/// </summary>
public class Data
{ 
    public static int Score;
    public static int defaultScore = 0; //default score 
    public static int addScore = 10; // add score value
    public static int minusScore = 15; // minus score value
}
