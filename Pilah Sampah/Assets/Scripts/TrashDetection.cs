﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using TMPro;
using UnityEngine;


/// <summary>
/// This class handle trash detection 
/// </summary>
public class TrashDetection : MonoBehaviour
{
    [SerializeField] private string nameTag;
    [SerializeField] private AudioClip correctAudio;
    [SerializeField] private AudioClip falseAudio;
    private AudioSource CorrectSound;
    private AudioSource FalseSound;

    [SerializeField] private TextMeshProUGUI textScore;

    private void Start()
    {
        CorrectSound = gameObject.AddComponent<AudioSource>(); // get audio source component
        CorrectSound.clip = correctAudio; // assign the audio clip
        
        FalseSound = gameObject.AddComponent<AudioSource>(); // get audio source component 
        FalseSound.clip = falseAudio; // assign the audio clip
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals(nameTag)) // check the gameobject tag
        {
            Data.Score += Data.addScore; // add score 
            textScore.text = Data.Score.ToString(); // convert the score to string 
            Destroy(other.gameObject); // destroy the gamebject when collide with the trashcan 
            CorrectSound.Play(); // play correct sound 
        }
        else
        {
            Data.Score -= Data.minusScore; // substract the score 
            textScore.text = Data.Score.ToString(); // convert the score to string 
            Destroy(other.gameObject); // destroy the gamebject when collide with the trashcan 
            FalseSound.Play(); // play false sound 
        }
    }
}

